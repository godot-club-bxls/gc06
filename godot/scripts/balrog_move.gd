extends Spatial

func _input(event):
	if event is InputEventKey:
		if event.pressed:
			if event.scancode == KEY_UP:
				$balrog.translation.z += 0.1
			elif event.scancode == KEY_DOWN:
				$balrog.translation.z -= 0.1
			elif event.scancode == KEY_RIGHT:
				$balrog.translation.x -= 0.1
			elif event.scancode == KEY_LEFT:
				$balrog.translation.x += 0.1
